#include <iostream>
#include <fstream>
#include "cds.h"
#include "dvdtype1.h"
using std::cout;
using std::cin;
using std::endl;


int dvdtype1:: selldvd1()
{//this function is for selling the stocks of the 3 different types of dvd
  //the structure is the same as the function present in cdtype.cpp
    if (option == 1)
    {
    ofstream myfile1;
    read.open("dvdstock1.txt");//this file stores the stocks for dvd type 1
    int line_number = 0;
    while (line_number != 2 && getline(read, line))//reads the second line from the code and stores it in "line"
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);//string is converted to int
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		read.close();


		myfile1.open("dvdstock1.txt", ios::out);
                myfile1 << "Here is the stock level for dvd type 1"<<endl;
                myfile1 << stocks ;
                myfile1.close();

		     //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



              }
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    else
      {
       std::cout<< "cannot read the second line" << endl;
      }

  
    }

    else if (option == 2)
      {//this has the same pattern as the above and the only difference is the file
	//the file differs for every type of item
         ofstream myfile1;
         read.open("dvdstock2.txt");
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
           {


             ++ line_number;
           }


         if (line_number == 2)
          {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {

                //this part runs after the quantity entered is less then the quanity in store at the moment
                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
                read.close();

		myfile1.open("dvdstock2", ios::out);
                myfile1 << "Here is the stock level for dvd type 2"<<endl;
                myfile1 << stocks ;
		myfile1.close();

		 //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



              }
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    else
      {
       std::cout<< "cannot read the second line" << endl;
      }

      }


    else if (option == 3)
      {//again follows the same pattern and the only difference is the name of the file 
	 ofstream myfile1;
         read.open("dvdstock3.txt");
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
           {


             ++ line_number;
           }


         if (line_number == 2)
          {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		read.close();

		myfile1.open("dvdstock3.txt",ios::out);
                myfile1 << "Here is the stock level for dvd type 3"<<endl;
                myfile1 << stocks ;
		myfile1.close();

		     //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }
              }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
	       std::cout<< "cannot read the second line" << endl;
      }

     
      }

    else
      {
        cout<< "dvd type not found"<<endl;
      }

   return 0;
  }


int dvdtype1::adddvd1()
{//first weopen the file depending on which type of dvd, then read the value and add it to the quantity entered by the user
  //and write the updated value back to the respective class

    if (option == 1)
      {
    read.open("dvdstock1.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("dvdstock1.txt", ios::out);
            myfile1 << "Here is the stock level for dvd type 1"<<endl;
            myfile1 << stocks ;
            myfile1.close();


	         //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }


	    
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

    else if (option == 2)
      {
    read.open("dvdstock2.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("dvdstock2.txt", ios::out);
            myfile1 << "Here is the stock level for dvd type 2"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	         //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }


	      }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

        else if (option == 3)
      {
    read.open("dvdstock3.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("dvdstock3.txt", ios::out);
            myfile1 << "Here is the stock level for dvd type 3"<<endl;
            myfile1 << stocks ;

	         //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }




	    
	       }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }
	
	else
	  
      {
        cout<< "dvd type not found"<<endl;
      }

   return 0;
  }

int dvdtype1:: managedvd1()
{//the pattern is same as that of add
  //only difference is we dont add the quantity, we replace it instead
   if (option == 1)
      {
    read.open("dvdstock1.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks =  qty;//here we replace the old value with that entered by the user
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("dvdstock1.txt", ios::out);
            myfile1 << "Here is the stock level for dvd type 1"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	         //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
	  {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

    else if (option == 2)
      {
    read.open("dvdstock2.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks =  qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("dvdstock2.txt", ios::out);
            myfile1 << "Here is the stock level for dvd type 2"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	         //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }




	    
              }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
	  }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

        else if (option == 3)
      {
    read.open("dvdstock3.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks =  qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("dvdstock3.txt", ios::out);
            myfile1 << "Here is the stock level for dvd type 3"<<endl;
            myfile1 << stocks ;

	         //here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "dvd type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }


	  }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }
      }
	      

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

        else

      {
        cout<< "dvd type not found"<<endl;
      }

   return 0;
  }

