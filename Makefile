CXX = g++
CXXFLAGS = -g -Wall -Wextra

program:projectMain.cpp cds.cpp cdtype1.cpp dvdtype1.cpp magazinetype1.cpp booktype1.cpp
	$(CXX) $(CXXFLAGS) -o program projectMain.cpp cds.cpp cdtype1.cpp dvdtype1.cpp magazinetype1.cpp booktype1.cpp

clean:
	$(RM)*.o
	$(RM) program
