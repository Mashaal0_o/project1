#include <iostream>
#include <fstream>
#include "cds.h"
#include "magazinetype1.h"
using std::cout;
using std::cin;
using std::endl;

int magazinetype1:: sellmagazine1()
{//this function is for selling the magazine stocks
    if (option == 1)
      {//if type 1 is selected then the below code is executed
    ofstream myfile1;
    read.open("magazinestock1.txt");//opens the file with the stock info
    int line_number = 0;
    while (line_number != 2 && getline(read, line))//reads the second line
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);//converts the string to int format
            if (stocks < qty )//if the user entered quantity is greatter then the available quantity it will print the following lines
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else//else the below code is executed
              {


                stocks = stocks - qty;//here we minus the values 
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		read.close();

		
                myfile1.open("magazinestock1.txt", ios::out);
                myfile1 << "Here is the stock level for magazine type 1"<<endl;
                myfile1 << stocks ;//here we write the updated value back to the file
		myfile1.close();

                //in the below few lines, we write to the stocks report file that changes were made with the time and date
                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



		
	       }
      

	  
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    else
      {
       std::cout<< "cannot read the second line" << endl;
      }

    }
    //same pattern is used here with different file for type 2

    else if (option == 2)
      {
         ofstream myfile1;
         read.open("magazinestock2.txt");
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
           {


             ++ line_number;
           }


         if (line_number == 2)
          {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		read.close();


		myfile1.open("magazinestock2.txt", ios::out);
                myfile1 << "Here is the stock level for magazine type 2"<<endl;
                myfile1 << stocks ;
		myfile1.close();

		time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }

                   

              }
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    else
      {
       std::cout<< "cannot read the second line" << endl;
      }

      
      }

    // same pattern again with a different file for type 3
    else if (option == 3)
      {
	 ofstream myfile1;
         read.open("magazinestock3.txt");
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
           {


             ++ line_number;
           }


         if (line_number == 2)
          {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
                read.close();

		myfile1.open("magazinestock3", ios::out);
                myfile1 << "Here is the stock level for magazine type 3"<<endl;
                myfile1 << stocks ;
		myfile1.close();

		time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }

              }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }
	  
	  

	  }
      else
      {
	       std::cout<< "cannot read the second line" << endl;
      }

      }

    else
      {
        cout<< "magazine type not found"<<endl;
      }

   return 0;
  }


int magazinetype1::addmagazine1()
{//same instructions as the sell function
  //only difference is we dont compare the two quantities and just add them together

    if (option == 1)
      {
    read.open("magazinestock1.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("magazinestock1.txt", ios::out);
            myfile1 << "Here is the stock level for magazine type 1"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	       time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }

	    

          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

    else if (option == 2)
      {
        read.open("magazinestock2.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))

          {

            ++ line_number;
          }


        if (line_number == 2)

         {

           try

             {

               int stocks = std::stoi(line);
               stocks = stocks + qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
	                   read.close();

            myfile1.open("magazinestock2.txt", ios::out);
            myfile1 << "Here is the stock level for magazine type 2"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }
      }
     else if (option == 3)
      {
        read.open("magazinestock3.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))

          {

            ++ line_number;
          }


        if (line_number == 2)

         {

           try

             {

               int stocks = std::stoi(line);
               stocks = stocks + qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
               read.close();

               myfile1.open("magazinestock3.txt", ios::out);
               myfile1 << "Here is the stock level for magazine type 3"<<endl;
               myfile1 << stocks ;
               myfile1.close();

	       time_t currenttime = time(0);
               char* DateandTime = ctime(&currenttime);
               ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }
      }
     else
       {
	 cout<<"magazine option not found";
       }
    return 0;
  }

int magazinetype1:: managemagazine1()
{//this function is used for double checking the stocks
  //here we completly replace the user quantity with the old quantity
   if (option == 1)
      {
    read.open("magazinestock1.txt");
    ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
	    
            stocks =  qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("magazinestock1.txt", ios::out);
	    myfile1 << "Here is the stock level for magazine type 1"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

    else if (option == 2)
      {
        read.open("magazinestock2.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))
	  
          {

            ++ line_number;
          }


        if (line_number == 2)

         {

           try

             {

               int stocks = std::stoi(line);
               stocks =  qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
                           read.close();

            myfile1.open("magazinestock2.txt", ios::out);
            myfile1 << "Here is the stock level for magazine type 2"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }
	   
        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }
      }
     else if (option == 3)
      {
        read.open("magazinestock3.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))

          {

            ++ line_number;
          }


        if (line_number == 2)
	  
         {

           try

             {

               int stocks = std::stoi(line);
               stocks =  qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
               read.close();

               myfile1.open("magazinestock3.txt", ios::out);
               myfile1 << "Here is the stock level for magazine type 3"<<endl;
               myfile1 << stocks ;
               myfile1.close();

	       time_t currenttime = time(0);
               char* DateandTime = ctime(&currenttime);
               ofstream stocksreport ("stockreport.txt", ios::app);

               if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "magazine type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      
	
      }

    else
      {
       std::cout<< "cannot read the second line" << endl;
      }
      }
     else
       {
         cout<<"magazine option not found";
       }
   return 0;
}







  
  

	
      
    





                   
