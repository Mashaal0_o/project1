#include <iostream>
#include <fstream>
#include <string>
#include "cds.h"
#include "cdtype1.h"
#include "dvdtype1.h"
#include "booktype1.h"
#include "magazinetype1.h"

using std::cout;
using std::cin;

int main()
{
  string username;
  string password;
  
  string name;
  int id;
  
  int option = 0;
  int type;
  int typess;
  int qty;
  int SellorAdd;

  ifstream read;
  string line = "";
  
  
  
  
  cout<< "Welcome to the M BoB store"<< endl; 
  cout<< "please login to access the menu"<< endl;
  cout<< "Username"<< endl;
  cin>> username;
  cout<< "password"<< endl;
  cin>> password;

  while (username != "admin" || password != "admin")//this while loop is to check and verify the entered password and username
  {
    cout<< "the entered username or password is incorrect"<< endl;
    cout<< "please enter the user name and password again"<< endl;
    cout<< "Username"<< endl;
    cin>> username;
    cout<< "password"<< endl;
    cin>> password;
  }

  
    

    

    while (option != -1)
  {

  cout<< "please select any of the following options: "<< endl;
  cout<< " 1: restock items, sell items"<< endl;
  cout<< " 2: manage items "<<endl;
  cout<< " 3: add new items "<<endl;
  cout<< " 4: view a report of the sales "<<endl;
  cout<< "enter -1 for exiting the menu"<<endl;
  //cin>> option;


  if (!( cin >> option))//this loop is to check if the user has entered an int or a string instead
   {
    cout<< "please enter a number not a string" <<endl;
    cin.clear();//clears the user input
    cin.ignore(200,'\n');
   }

  else
    {
      
      
    
  
    
      
 
    
       


  
    
  if (option == 1)
    {
      cout<< "which of the following item do you want to sell or add: "<<endl;
      cout<< " 1: cds "<<endl;
      cout<< " 2: books "<<endl;
      cout<< " 3: magazines "<<endl;
      cout<< " 4: dvds "<<endl;
      cin>> type;


      if (type == 1)
	{
	  cout<< "select the cd type you want to modify  : "<<endl;
	  cout<< " 1: Musicians in 10 days "<<endl;
	  cout<< " 2: best musicians tips "<<endl;
	  cout<< " 3: cdno3 "<<endl;
	  cin>> typess;

	  cout<< "do you want to sell or add selected type"<<endl;
	  cout<<" 1: add "<<endl;
	  cout<<" 2: sell "<<endl;

	  
	 
	  cin>> SellorAdd;

	 


	  cout<< "please enter the quanity"<<endl;
	  cin>> qty;

	  if ( typess == 1)
	    {
	      cdtype1 cd1{};//cd object is created
	      cd1.setoption(typess);//parent class function is used
	      cd1.setqty(qty);//to set the quantity
	      if (SellorAdd == 1)
		{
	          cd1.addcd1();//function for adding the quantity
		}
	      else if (SellorAdd == 2)
		{
		  cd1.sellcd1();//function for selling the quantity, we call the sellcd1 function from the cdtype1 class
		  
		}
	      else
		{
		  cout<<"please enter a valid entry"<<endl;
		}
	    }
	  else if (typess == 2)
	    {
	      cdtype1 cd2{};//an object of type two is created
	      cd2.setoption(typess);//passes the typess variable to be set for further use
	      cd2.setqty(qty);//passes quntity and sets it
	        if (SellorAdd == 1)
                {
                  cd2.addcd1();//calls the addcd1 function from cdtype1 class
                }
              else if (SellorAdd == 2)
                {
                  cd2.sellcd1();//calls the sellcd function from cdtype1 class
                }
              else
                {
                  cout<<"please choose a valid entry"<<endl;
                }

	      
	    }
	  else if (typess == 3)
	    {
	      cdtype1 cd3{};//an object of cd type 3 is created
	      cd3.setoption(typess);//typess varibale is set
	      cd3.setqty(qty);//quantity is set here by calling the setqty in the parent class cds
	      if (SellorAdd == 1)
                {
                  cd3.addcd1();//calls the addcd1 function from cdtype1 class
                }
              else if (SellorAdd == 2)
                {
                  cd3.sellcd1();//calls the sellcd1 function from the cdtype1 class
                }
              else
                {
                  cout<<"please enter a valid option"<<endl;
                }

	    }
	  else
	    {
	      cout<<"error in loading the types"<<endl;
	      cout<<"please try again"<<endl;
	    }
	}
      else if (type == 2)
	{
	  cout<< "select the book type you want to modify: "<<endl;
          cout<< " 1: book type 1 "<<endl;
          cout<< " 2: book type 2 "<<endl;
          cout<< " 3: book type 3 "<<endl;
          cin>> typess;

	  cout<< "do you want to sell or add the selected type"<<endl;
          cout<<" 1: add "<<endl;
          cout<<" 2: sell "<<endl;
          cin>> SellorAdd;


          cout<< "please enter the quanity"<<endl;
          cin>> qty;

          if ( typess == 1)
            {
              booktype1 book1{};//here an object is made of the booktype1 class
              book1.setoption(typess);//here we set the typess of book option using the function in cds.cpp
              book1.setqty(qty);//again setting the quantity using the function in cds.cpp
              if (SellorAdd == 1)
                {
                  book1.addbook1();//here we call the addbook1 function from the class booktype1 if the user chooses to add stocks
                }
              else if (SellorAdd == 2)
                {
                  book1.sellbook1();//else we call the sellbook1 function from the booktype1 class
                }
              else
                {
                  cout <<"please enter a valid option"<<endl;
                }

            }
          else if (typess == 2)
            {
              booktype1 book2{};//here an object is created of the booktype1 class
              book2.setoption(typess);//here we set the types of book 
              book2.setqty(qty);//here we set the quantity
              if (SellorAdd == 1)
                {
                  book2.addbook1();//here we call the addbook1 function from the class booktype1 if the user chooses to add stocks
                }
              else if (SellorAdd == 2)
                {
                  book2.sellbook1();//else we call the sellbook1 function
                }
              else
                {
                  cout <<"please enter a valid option"<<endl;
                }

            }
          else if (typess == 3)
            {
              booktype1 book3{};//here we create another object if the user selectes type 3 book
              book3.setoption(typess);//we set the typess entered by the user
              book3.setqty(qty);//here we set the quantity entered by the user
              if (SellorAdd == 1)
                {
                  book3.addbook1();//here we call the addbook1 function from the class booktype1 if the user chooses to add stocks
                }
              else if (SellorAdd == 2)
                {
                  book3.sellbook1();//else if user asks to sell, then we call the sellbook function
                }
              else
                {
                  cout <<"please enter a valid option"<<endl;
                }

            }
          else
            {
              cout<<"error in loading the types"<<endl;
              cout<<"please try again"<<endl;
	    }
	}
      else if (type == 3)//if the user enters type 3 which is magazine then this part of the code runs
	{
	  cout<< "select the type of magazine you want to modify: "<<endl;
          cout<< " 1: Magazine 1 "<<endl;
          cout<< " 2: magazine 2 "<<endl;
          cout<< " 3: magazine 3 "<<endl;
          cin>> typess;

	  cout<< "do you want to sell or add the selected type"<<endl;
          cout<<" 1: add "<<endl;
          cout<<" 2: sell "<<endl;
          cin>> SellorAdd;


          cout<< "please enter the quanity"<<endl;
          cin>> qty;

          if ( typess == 1)//if user enters 1 for the typess, then this part of the code is executed
            {
              magazinetype1 magazine1{};//an object is created of class magazinetype1
              magazine1.setoption(typess);//here we set the typess calling the setoption function22
              magazine1.setqty(qty);//here we set the quantity using the setqty function
              if (SellorAdd == 1)
                {
                  magazine1.addmagazine1();//if the user enters 1, then the addmagazine1 function is called
                }
              else if (SellorAdd == 2)
                {
                  magazine1.sellmagazine1();//else if the user enters 2,then the sellmagazine function is called
                }
              else
                {
                  cout<< "please enter a valid option"<<endl;//else if the number is more than 2,it will print this statement 
                }

            }
          else if (typess == 2)//if the user enters 2 for type 2 then this part of the code is executed
            {
              magazinetype1 magazine2{};//an object is created of class magazinetype1
              magazine2.setoption(typess);//here we set the type of magzine choosen
              magazine2.setqty(qty);//here we set the quantity
              if (SellorAdd == 1)//if the user wants to add stocks
                {
                  magazine2.addmagazine1();//addmagazine1 function is called
                }
              else if (SellorAdd == 2)//else if the user wants to sell 
                {
                  magazine2.sellmagazine1();//sellmagazine1 function is called
                }
              else
                {
                  cout<< "please enter a valid option"<<endl;
                }

            }
          else if (typess == 3)//if the user enters 3 for type 3 then this part of the code is executed

            {
              magazinetype1 magazine3{};//object is created 
              magazine3.setoption(typess);//the typess entered by the user is set
              magazine3.setqty(qty);//the quantity is set here
              if (SellorAdd == 1)//if the user chooses to add stocks
                {
                  magazine3.addmagazine1();//addmagazine function is called
                }
              else if (SellorAdd == 2)//else if user chooses to sell stocks
                {
                  magazine3.sellmagazine1();//the sellmagazine1 function is called
                }
              else
                {
                  cout<<"please enter a valid option"<<endl;//if the user doesnt enter 1 or 2, this is printed
                }

            }
          else//if the user doesnt pick a type out of the menu the following lines are printed
            {
              cout<<"error in loading the types"<<endl;
              cout<<"please try again"<<endl;
	    }
	}
      else if (type == 4)//if the user enters 4,to deal with dvds this part of the code is executed
	{
	  cout<< "select the dvd type you want to modify: "<<endl;
          cout<< " 1: dvd type 1 "<<endl;
          cout<< " 2: dvd type 2 "<<endl;
          cout<< " 3: dvd type 3 "<<endl;
          cin>> typess;//user is asked to enter a type and its stored here

	  cout<< "do you want to sell or add the selected type"<<endl;
          cout<<" 1: add "<<endl;
          cout<<" 2: sell "<<endl;
          cin>> SellorAdd;//user is asked to sell or add and it stores the input


          cout<< "please enter the qty"<<endl;
	  cin>> qty;//here we store the quantity entered by the user

          if ( typess == 1)//if the user enters type 1, this part of the code is executed
            {
              dvdtype1 dvd1{};//object of class dvdtype1 is created
              dvd1.setoption(typess);//the type of magazine is set here
              dvd1.setqty(qty);//the quantity is set here
              if (SellorAdd == 1)
                {
                  dvd1.adddvd1();//if the user wishes to add then this function is called
                }
              else if (SellorAdd == 2)
                {
                  dvd1.selldvd1();//else if user enters 2 for selling, the selldvd function is called
                }
              else
                {
                  cout<<"please enter a valid option"<<endl;//else if neither 1 or 2 are entered then this line is executed
                }

            }
          else if (typess == 2)//if the user enters type 2, this part of the code is executed
            {
              dvdtype1 dvd2{};//an object is created of class dvdtype1
              dvd2.setoption(typess);//the typess entered is set here
              dvd2.setqty(qty);//the quantity entered by the user is set here
	      if (SellorAdd == 1)
                {
                  dvd2.adddvd1();//if the user selects to add then adddvd1 function is called
                }
              else if (SellorAdd == 2)
                {
                  dvd2.selldvd1();//if the user wishes to sell then this selldvd1 function is called
                }
              else
                {
                  cout<<"please enter a valid option"<<endl;//if nor 1 nor 2 is selected then this line is printed
                }

            }
          else if (typess == 3)//if the user enters type 3, this part of the code is executed
            {
              dvdtype1 dvd3{};//object is created for thethird type
              dvd3.setoption(typess);//the type od dvd is set here using the setoption function
              dvd3.setqty(qty);//the quantity is set using the sellqty function
              if (SellorAdd == 1)
                {
                  dvd3.adddvd1();//if the user wishes to add stock then adddvd1 function is called
                }
              else if (SellorAdd == 2)
                {
                  dvd3.selldvd1();//else if the user enters to sell, selldvd function is called
                }
              else
                {
                  cout<<"please enter a valid option"<<endl;//else if none 1 or 2 then this line is printed
                }

            }
	    else//if the user is unable to pick the given types then these lines are printed
            {
              cout<<"error in loading the types"<<endl;
              cout<<"please try again"<<endl;
	    }
	}
  else//if the user isn't able to chose the given items then this line is printed
	{
	  cout<<"please enter a logical answer";
	}
    }
    else if (option == 2)//if the user wishes to manage the stocks, this section of the code is executed
    {

      cout<< "which of the following item stocks do you want to edit: "<<endl;
      cout<< " 1: cds "<<endl;
      cout<< " 2: books "<<endl;
      cout<< " 3: magazines "<<endl;
      cout<< " 4: dvds "<<endl;
      cin>> type;//the type entered by the user is stored here



      
      if (type == 1)//if the user enters 1 for type 1, then this part is executed
        {
	
	   	  
           cout<< "select the cd type you want to edit: "<<endl;
           cout<< " 1: Musicians in 10 days "<<endl;
           cout<< " 2: best musicians tips "<<endl;
           cout<< " 3: cdno3 "<<endl;
           cin>> typess;//stores the typeof cd entered by the user
	  
          

           if ( typess == 1)//if user wantsto manage stocks of type 1 then the following code is executed
            {
	      read.open("cdstock1.txt");//we open the stocksreport file of cd type 1
               int line_number = 0;
               while (line_number != 2 && getline(read, line))//this while loop is used for counting the lines and stops on the second line and reads it
                 {
                   ++ line_number;//keeps adding 1 until line_number is 2
                 }
	       read.close();//closes the file after reading the stocks
               int stocks = std::stoi(line);//converts the string form to integer for calculations
               cout << "current stock levels are: ";
               cout << stocks << endl;
	       cout<< "please enter the quanity "<<endl;
               cin>> qty;//the new quantity entered by the user is stored here


	      
	       cdtype1 cd1{};//object is created for type 1 of cd of the cdtype1 class
	       cd1.setoption(typess);//the types option is set here
	       cd1.setqty(qty);//the quantity entered by the user is set here
	       cd1.managecd1();//the managecd1 function is called to change the stocks level for type 1 of cd
	      //the same pattern is used for the remaing types and different items
            }
          else if (typess == 2)//if user wantsto manage stocks of type 2 then the following code is executed
            {
	      read.open("cdstock2.txt");//the stocks file for type 2 of cds is opened here
               int line_number = 0;
               while (line_number != 2 && getline(read, line))//this while loop is used for counting the lines and stops on the second line and reads it
                 {
                   ++ line_number;//keeps adding 1 until the value is not 2
                 }
               read.close();//after reading the stocks it closes the file
               int stocks = std::stoi(line);//converts the stocks from string to int
               cout << "current stock levels are: ";
               cout << stocks << endl;//prints the current stock level
               cout<< "please enter the quanity"<<endl;
               cin>> qty;//asks the user to entered the updated stock level

	      
	       cdtype1 cd2{};//object is created for type 2 of cd
	       cd2.setoption(typess);//the type of cd is set here
	       cd2.setqty(qty);//the quantity entered by the user is set here
	       cd2.managecd1();//the managecd1 function is called from cdtype1 class
            }
          else if (typess == 3)//if the user chooses type 3 of cd, this part is executed
            {
	      read.open("cdstock3.txt");//stocks report file for cd type 3 is opened here
               int line_number = 0;
               while (line_number != 2 && getline(read, line))//this while loop is used for counting the lines and stops on the second line and reads it
                 {
                   ++ line_number;
                 }
               read.close();//reads the current stocks and closes the file 
               int stocks = std::stoi(line);//converts the stored stocks value from string to int formate
               cout << "current stock levels are: ";
               cout << stocks << endl;//prints the current stock level
               cout<< "please enter the quanity"<<endl;
               cin>> qty;//asks for the updated stock levels and stores it in quantity

	       cdtype1 cd3{};//object for type 3 is created
	       cd3.setoption(typess);
	      cd3.setqty(qty);
              cd3.managecd1();
            }
          else//if the user fails to enter any of the above types then these lines are executed
            {
              cout<<"error in loading the types"<<endl;
              cout<<"please try again"<<endl;
            }
        }
      else if (type == 2)//if the user enters 2 for books, this part of the code is executed
        {
          cout<< "select the book tpe you want to edit: "<<endl;
          cout<< " 1: book type 1 "<<endl;
          cout<< " 2: book type 2 "<<endl;
          cout<< " 3: book type 3 "<<endl;
          cin>> typess;//stores the type entered by the user

          

          if ( typess == 1)//if user enters 1
            {
	      read.open("bookstock1.txt");//the stockfile for booktype 1 is opened 
               int line_number = 0;
               while (line_number != 2 && getline(read, line))//this while loop is used for counting the lines and stops on the second line and reads the stock level
                 {
                   ++ line_number;
                 }
               read.close();//closes the file after reading the stocks 
               int stocks = std::stoi(line);//converts the stored string value to int 
               cout << "current stock levels are: ";
               cout << stocks << endl;//prints the current stocks level
               cout<< "please enter the quanity"<<endl;
               cin>> qty;//stores the enetered quantity

	       booktype1 book1{};//object is created of class booktype1
	       book1.setoption(typess);//the types selected by the usr is set here
	       book1.setqty(qty);//the quantity entered by the user is set here
	       book1.managebook1();//the managebook1 function is called from the cdtype1 class
	       
            }
          else if (typess == 2)//if the user enters 2 for book type 2
            {

	      read.open("bookstock2.txt");//the stock file for book type 2 is opened here
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;

              booktype1 book2{};
              book2.setoption(typess);
              book2.setqty(qty);
              book2.managebook1();
            }
          else if (typess == 3)
            {
	       read.open("bookstock3.txt");
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;

              booktype1 book3{};
	      book3.setoption(typess);
              book3.setqty(qty);
              book3.managebook1();
            }
          else
            {
              cout<<"error in loading the types"<<endl;
              cout<<"please try again"<<endl;
            }
        }
      else if (type == 3)//this is when the user enters 3 for magazines
        {
          cout<< "select the type of magazine you want to edit: "<<endl;
          cout<< " 1: Magazine 1 "<<endl;
          cout<< " 2: magazine 2 "<<endl;
          cout<< " 3: magazine 3 "<<endl;
          cin>> typess;

          

          if ( typess == 1)
            {
	      read.open("magazinestock1.txt");//magazine type 1 stock report is opened becase it stores the current stocks value
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;


	      
              magazinetype1 magazine1{};
              magazine1.setoption(typess);
              magazine1.setqty(qty);
              magazine1.managemagazine1();
            }
          else if (typess == 2)
            {

	       read.open("magazinestock2.txt");//magazine type 2 stock report is opened becase it stores the current stocks value
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;

              magazinetype1 magazine2{};
              magazine2.setoption(typess);
              magazine2.setqty(qty);
              magazine2.managemagazine1();
            }
          else if (typess == 3)
            {

	       read.open("magazinestock3.txt");//magazine type 3 stock report is opened becase it stores the current stocks value
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;

              magazinetype1 magazine3{};
	         magazine3.setoption(typess);
              magazine3.setqty(qty);
              magazine3.managemagazine1();
            }
    
          else
            {
              cout<<"error in loading the types"<<endl;
              cout<<"please try again"<<endl;
            }
        }
      else if (type == 4)//if the user enters 4 for dvd
        {
           cout<< "select the dvd type you want to edit: "<<endl;
          cout<< " 1: dvd type 1 "<<endl;
          cout<< " 2: dvd type 2 "<<endl;
          cout<< " 3: dvd type 3 "<<endl;
          cin>> typess;

        

          if ( typess == 1)
            {
	       read.open("dvdstock1.txt");//dvd type 1 stock report is opened becase it stores the current stocks value
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;

              dvdtype1 dvd1{};
              dvd1.setoption(typess);
              dvd1.setqty(qty);
              dvd1.managedvd1();
            }
          else if (typess == 2)
            {

	       read.open("dvdstock2.txt");//dvd type 2 stock report is opened becase it stores the current stocks value
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;

              dvdtype1 dvd2{};
	      dvd2.setoption(typess);
              dvd2.setqty(qty);
              dvd2.managedvd1();
            }
          else if (typess == 3)
            {
	       read.open("dvdstock3.txt");//dvd type 3 stock report is opened becase it stores the current stocks value
               int line_number = 0;
               while (line_number != 2 && getline(read, line))
                 {
                   ++ line_number;
                 }
               read.close();
               int stocks = std::stoi(line);
               cout << "current stock levels are: ";
               cout << stocks << endl;
               cout<< "please enter the quanity"<<endl;
               cin>> qty;

              dvdtype1 dvd3{};
              dvd3.setoption(typess);
              dvd3.setqty(qty);
              dvd3.managedvd1();
            }
          else
            {
              cout<<"error in loading the types"<<endl;
              cout<<"please try again"<<endl;
            }
        }
      else
        {
          cout<<"please enter a logical answer";//this is printed when the user doesnt enter a number from 1-4
        }
    }

    else if (option == 3)//this part of the code is to create a new item
    {
      
      cout << " Please enter the name of the new item: "<< endl;
      cin >> name;
      cout << " please enter the id of the item: "<<endl;
      cin >> id;
      cout << "please enter the qty of this item: "<<endl;
      cin >> qty;

      cds newitem{};//object
      newitem.setname(name);//sets the name
      newitem.setid(id);//sets the id
      newitem.setqty(qty);//sets the qty
      newitem.addNewItem();//the function is called to create a new object
     
    }
    else if (option == 4)//this function is when the user asks for the stock report
    {
      
      string lines;
      ifstream reader;
      reader.open("stockreport.txt");
      while(getline(reader, lines))//this function prints the stockreport.txt file line by line
	{
	  cout << lines << endl;
	}
      cout << "the information is avaliable in the stockreport.txt file"<<endl;

    }
    }




  }
    
    
 

    
   
    
    
    
  cout<< "Thankyou"<< endl;
  }

       






       


    
        








      



	  
	  
	      
    
    


	  

      
  
		
  


