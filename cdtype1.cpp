#include <iostream>
#include <fstream>
#include "cds.h"
#include "cdtype1.h"
using std::cout;
using std::cin;
using std::endl;


int cdtype1:: sellcd1()
  {
    if (option == 1)
    {
    ofstream myfile1;
    read.open("cdstock1.txt");//cdsstocks1.txt file is opened for reading operation
    int line_number = 0;
    while (line_number != 2 && getline(read, line))//this function is read the 2nd line from the file
      {
	++ line_number;
      }

    if (line_number == 2)
      {
	try
	  {
	    int stocks = std::stoi(line);//converts string to int
	    if (stocks < qty )//checks if the present quantity is less than that entered by the user
	      {
		cout << "sorry you dont have sufficent stocks"<<endl;
		cout << "current stocks = "<<endl;
		cout <<  stocks <<endl;
		
	      }
	    else//if the current qauantity is more then that enered by the user then the below code is used
	      {
	      
	 
		stocks = stocks - qty;
		cout << "transection was succesful"<<endl;
		cout << "current stocks level is: "<<endl;
		cout<<  stocks << endl;
		read.close();
	       
                myfile1.open("cdstock1.txt");//here we open cdstck1 file in writing format
		myfile1 << "here is the stock level for cd type 1"<<endl;
		myfile1 << stocks ;
		myfile1.close();
	       
		//here we write the updates to the stock report file and include the date and time
                time_t currenttime = time(0);
		char* DateandTime = ctime(&currenttime);
		ofstream stocksreport ("stockreport.txt", ios::app);

		if(stocksreport.is_open())
		  {
		    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
		    stocksreport << "cd type 1, " << stocks << endl;
		    stocksreport.close();
		  }
		else
		  {
		    cout << "cannot open the stocks report"<<endl;
		  }
		

		

	      }
	  }
	
      	catch(int stockkevels)
	  {
	    std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
	  }
      
      }
    else
      {
       std::cout<< "cannot read the second line :(" << endl;
      }

    
		    
   
    }

    else if (option == 2)
      {//this part is if the user wants type 2 of cds
	
	 ofstream myfile1 ("cdstock2.txt");
         read.open("cdstock2.txt");
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
	   {
	     
             
             ++ line_number;
           }
	 

         if (line_number == 2)
          {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;

                if (myfile1.is_open())

                  {

                    myfile1 << "Here is the stock level for cd type 2"<<endl;
                    myfile1 << stocks ;
		     }
                else

                  {
                    cout << "cnnot write"<<endl;
                  }

		         myfile1.close();
                read.close();

                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 2, " << stocks << endl;
		    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }

	      }

	        
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    else
      {
       std::cout<< "cannot read the second line" << endl;
      }

     
      }


    else if (option == 3)
      {//if the user selects the third type of cd
	 ofstream myfile1;
         read.open("cdstock3.txt");
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
           {


             ++ line_number;
           }


         if (line_number == 2)
          {
        try
          {
	    read.close();
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {

		myfile1.open("cdstock3.txt", ios::out);


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		myfile1 << "Here is the stock level for cd type 3"<<endl;
		myfile1 << stocks ;
		myfile1.close();
	      }

	        

	        
               

                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 3, " << stocks << endl;
		    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }




                     

	  
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
       std::cout<< "cannot read the second line:)" << endl;
      }

   
      }

    else
      {
	cout<< "cd type not found"<<endl;
      }

   return 0;
  }    


int cdtype1::addcd1()
{//this function is to add the stocks
  //same like the sell function but here instead of subtracting we add the two quantities

    if (option == 1)
      {
    read.open("cdstock1.txt");
    ofstream myfile1;
    
   
    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
	    read.close();

	    myfile1.open("cdstock1.txt", ios::out);
	    myfile1 << "Here is the stock level for cd type 1"<<endl;
	    myfile1 << stocks ;
	    myfile1.close();

	        
            

                time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 1, " << stocks << endl;
		    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



              
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    
    else
      {
       std::cout<< "cannot read the second line :)" << endl;
      }
       

      }

    else if (option == 2)
      {
	read.open("cdstock2.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))
	  
          {
	    
            ++ line_number;
          }
	

        if (line_number == 2)
	  
         {
	   
           try
	     
             {
	       
               int stocks = std::stoi(line);
               stocks = stocks + qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
	       read.close();
		   
               myfile1.open("cdstock2.txt", ios::out);
               myfile1 << "Here is the stock level for cd type 2"<<endl;
               myfile1 << stocks ;
	       myfile1.close();

	         time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 2, " << stocks << endl;
		    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

	 }
	
        else
      {
	 std::cout<< "cannot read the second line" << endl;
      }
	

        
      }

    else if (option == 3)
      {
	read.open("cdstock3.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))

          {

            ++ line_number;
          }


        if (line_number == 2)

         {

           try

             {

               int stocks = std::stoi(line);
               stocks = stocks + qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
	       read.close();

	       myfile1.open("cdstock3");
               myfile1 << "Here is the stock level for cd type 3"<<endl;
               myfile1 << stocks <<endl; ;
	       myfile1.close();

	         time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 3, " << stocks << endl;
		    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
	
        else
	  
        {
	  
          std::cout<< "cannot read the second line" << endl;
	  
        }

      }
    

    else
      
      {
	cout<<"cannot access the options"<<endl;
      }

   return 0;
  }
int cdtype1::managecd1()
{//this function is forchecking the stock level in record compared to the actual and if there are any changes you can just enter the new stocks level
    if (option == 1)
      {
        read.open("cdstock1.txt");
        ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
	    
            int stocks = std::stoi(line);
	    cout << "the current stock level is: ";
	    cout << stocks << endl;
            stocks =  qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("cdstock1.txt", ios::out);
            myfile1 << "Here is the stock level for cd type 1"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	      time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 1, " << stocks << endl;
		    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
        else
      {
       std::cout<< "cannot read the second line" << endl;
      }


      }

    else if (option == 2)
      {
        read.open("cdstock2.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))

          {

            ++ line_number;
          }


        if (line_number == 2)

         {

           try

             {

               int stocks = std::stoi(line);
	       cout << "the current stock level is: ";
               cout << stocks << endl;

               stocks =  qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
               read.close();

               myfile1.open("cdstock2.txt", ios::out);
               myfile1 << "Here is the stock level for cd type 2"<<endl;
               myfile1 << stocks ;
               myfile1.close();

	          time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }





	       
	                 }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

         }

        else
      {
         std::cout<< "cannot read the second line" << endl;
      }



      }

    else if (option == 3)
      {
        read.open("cdstock3.txt");
        ofstream myfile1;
        int line_number = 0;
        while (line_number != 2 && getline(read, line))

          {

            ++ line_number;
          }


        if (line_number == 2)

         {

           try

             {
	       
               int stocks = std::stoi(line);
	       cout << "the current stock level is: ";
               cout << stocks << endl;

               stocks =  qty;
               cout << "transection was succesful"<<endl;
               cout << "current stocks level is: "<<endl;
               cout<<  stocks << endl;
               read.close();

               myfile1.open("cdstock3", ios::out);
               myfile1 << "Here is the stock level for cd type 3"<<endl;
               myfile1 << stocks <<endl; ;
               myfile1.close();

	        time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "cd type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }

        else

        {

          std::cout<< "cannot read the second line" << endl;

        }

      }


    else

      {
        cout<<"cannot access the options"<<endl;
      }
    return 0;
}







  



