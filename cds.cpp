#include <iostream>
#include "cds.h"
#include "cdtype1.h"
#include "dvdtype1.h"
#include "magazinetype1.h"
#include "booktype1.h"

using std::cout;
using std::cin;
using std::endl;


void cds::setid(int num)
  {
    id = num;
  }
void cds::setoption(int opt)
{
  option = opt;
}

void cds::setqty(int qtyy)
  {
    qty = qtyy;
  }

void cds::setname(string nameOftheitem)
  {
    name = nameOftheitem;
  }

int cds::addNewItem()
{
  time_t currentTime = time(0);//this sets the current time of the day
  char* dateAndtime = ctime(&currentTime);//this adds the date and time together
  
  ofstream writer("stockreport.txt", ios::app );//opens the file in append mode
  if(writer.is_open())
    {
      writer << "\nnew item added on the " << dateAndtime; "\n" ;
      writer << name << ", " << id << ", " << qty << endl;//here the name, id and qty is written to the stockreport.txt file 
      writer.close();
    }
  else
    {
      cout<< "cannot open the file"<<endl;
    }
  return 0;
}
  
      
  
  



  
