#include <iostream>
#include <fstream>
#include "cds.h"
#include "booktype1.h"
using std::cout;
using std::cin;
using std::endl;



int booktype1:: sellbook1()
{//this function is for selling the book stocks
  if (option == 1)//this applies to type 1 of books 
    {
    ofstream myfile1;
    read.open("bookstock1.txt");//this file contains the current stock value
    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
		
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		read.close();


		myfile1.open("bookstock1.txt", ios::out);
		myfile1 << "Here is the stock level for books type 1"<<endl;
                myfile1 << stocks ;//here we update the stock level in the file
		myfile1.close();

		time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }


              }
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    else
      {
       cout<< "cannot read the second line" << endl;
      }

 
    }

    else if (option == 2)
      {//this applies to the 2nd type of books
	ofstream myfile1;
	read.open("bookstock2.txt");//this filecontains the current stock level
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
           {


             ++ line_number;
           }


         if (line_number == 2)
          {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		read.close();

		myfile1.open("bookstock2.txt", ios::out);
                myfile1 << "Here is the stock level for book type 2"<<endl;
                myfile1 << stocks ;
                myfile1.close();

		time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }


              }
          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
    else
      {
       std::cout<< "cannot read the second line" << endl;
      }

    
      }


    else if (option == 3)
      {//tihs applies to book type 3
	 ofstream myfile1;
         read.open("bookstock3.txt");//this file contains the stock levels for the third type
         int line_number = 0;
         while (line_number != 2 && getline(read, line))
           {


             ++ line_number;
           }


         if (line_number == 2)
          {
        try
          {
            int stocks = std::stoi(line);
            if (stocks < qty )
              {
                cout << "sorry you dont have sufficent stocks"<<endl;
                cout << "current stocks = "<<endl;
                cout <<  stocks <<endl;
              }
            else
              {


                stocks = stocks - qty;
                cout << "transection was succesful"<<endl;
                cout << "current stocks level is: "<<endl;
                cout<<  stocks << endl;
		read.close();


		myfile1.open("bookstock3.txt", ios::out);
                myfile1 << "Here is the stock level for book type 3"<<endl;
                myfile1 << stocks ;
		myfile1.close();

		time_t currenttime = time(0);
                char* DateandTime = ctime(&currenttime);
                ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }

              }



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
	       std::cout<< "cannot read the second line" << endl;
      }

      
      }

    else
      {
        cout<< "book type not found"<<endl;
      }

   return 0;
  }


int booktype1::addbook1()
{//this function is for adding the stocks

    if (option == 1)
      {
        read.open("bookstock1.txt");//this file contains the stock level for book type 1
        ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("bookstock1.txt", ios::out);
            myfile1 << "Here is the stock level for book type 1"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 1, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }

	        



          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
               std::cout<< "cannot read the second line" << endl;
      }
  

      
      }
    else if (option == 2)
      {//this part applies for book type 2
	 read.open("bookstock2.txt");
         ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("bookstock2.txt", ios::out);
            myfile1 << "Here is the stock level for book type 2"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }






	    
	   }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
               std::cout<< "cannot read the second line" << endl;
      }



      }
    else if (option == 3)
      {//this part is for book type 3
	 read.open("bookstock3.txt");
         ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks = stocks + qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("bookstock3.txt", ios::out);
            myfile1 << "Here is the stock level for books type 3"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }




	    
         }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
               std::cout<< "cannot read the second line" << endl;
      }



      }
    else
      {
	cout<<"book types not found";
      }
    return 0;
  }

int booktype1::managebook1()
{//this function is for double checking the current stock levels

    if (option == 1)
      {
        read.open("bookstock1.txt");//opened this file to read the current stocks
        ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))//reads the second line of the file only
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);//converts string to int format
            stocks =  qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("bookstock1.txt", ios::out);
            myfile1 << "Here is the stock level for book type 1"<<endl;
            myfile1 << stocks ;//writes the correct value back to the file
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);//here we open the stocks report file

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 1, " << stocks << endl;//and write to the file in append mode 
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }





          }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
          else
      {
               std::cout<< "cannot read the second line" << endl;
      }



      }
    else if (option == 2)
      {//this applies to the second type of the book
         read.open("bookstock2.txt");
         ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks =  qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("bookstock2.txt", ios::out);
            myfile1 << "Here is the stock level for book type 2"<<endl;
            myfile1 << stocks ;
            myfile1.close();

	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 2, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }




	    
              }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
               std::cout<< "cannot read the second line" << endl;
	             }



      }
    else if (option == 3)
      {//this applies to the third type of books
         read.open("bookstock3.txt");
         ofstream myfile1;


    int line_number = 0;
    while (line_number != 2 && getline(read, line))
      {
        ++ line_number;
      }

    if (line_number == 2)
      {
        try
          {
            int stocks = std::stoi(line);
            stocks =  qty;
            cout << "transection was succesful"<<endl;
            cout << "current stocks level is: "<<endl;
            cout<<  stocks << endl;
            read.close();

            myfile1.open("bookstock3.txt", ios::out);
            myfile1 << "Here is the stock level for books type 3"<<endl;
            myfile1 << stocks ;
            myfile1.close();


	    time_t currenttime = time(0);
            char* DateandTime = ctime(&currenttime);
            ofstream stocksreport ("stockreport.txt", ios::app);

                if(stocksreport.is_open())
                  {
                    stocksreport << "\nstocks have been updated on the "<< DateandTime;"\n";
                    stocksreport << "book type 3, " << stocks << endl;
                    stocksreport.close();
                  }
                else
                  {
                    cout << "cannot open the stocks report"<<endl;
                  }







	    
              }

        catch(int stockkevels)
          {
            std::cerr << "ERROR PLEASE TRY AGAIN" <<endl;
          }

      }
      else
      {
               std::cout<< "cannot read the second line" << endl;
      }
    
      }
    else
      {
        cout<<"book types not found";
      }
    return 0;
  }













  





