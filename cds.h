#pragma once// we use this so that this file is only defined once
#include <iostream>
#include <fstream>
#include <ctime>//for the time
using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
using std::ios;


class cds
{
public:
  int id;
  string name;
  int qty;
  int option;

  void setid(int num);//sets the id
  void setoption(int opt);//sets the option
  void setqty(int qtyy);//sets the qty
  void setname(string nameOftheitem);//sets the name of the item

  int getid() { return id; }
  int getqty() { return qty; }
  string getname() { return name; }
  int addNewItem();//this function is for adding a new item

};


